﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wuhyuo
{
    /// <summary>
    /// GraphRenderer.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class GraphRenderer : UserControl
    {
        public static readonly DependencyProperty TextProperty
            = DependencyProperty.Register("Text", typeof(string), typeof(GraphRenderer), new UIPropertyMetadata(""));
        public static readonly DependencyProperty StrokeProperty
            = DependencyProperty.Register("Stroke", typeof(Brush), typeof(GraphRenderer), new UIPropertyMetadata(Brushes.Black));

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }
        public Brush Stroke
        {
            get { return (Brush)GetValue(StrokeProperty); }
            set { SetValue(StrokeProperty, value); }
        }

        public double Time { get; private set; } = 0;

        public GraphRenderer()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, EventArgs e)
        {
            label.Content = Text;
            polyline.Stroke = Stroke;
            polyline.StrokeThickness = 2;
        }

        public void AddValue(double dt, double value)
        {
            Time += dt;
            double x = 50 * Time;
            double y = 10 * value;

            polyline.Points.Add(new Point(x, y));
            translate.X = -x;
        }
    }
}
