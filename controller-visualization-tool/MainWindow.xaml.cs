﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.IO.Ports;
using System.IO;
using System.Windows.Media.Media3D;

namespace wuhyuo
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, EventArgs e)
        {
            _thrd = new Thread(InputThread);
            _thrd.Start();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            _thrdStop = true;
            _thrd?.Join();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            viewRotate.Quaternion = Quaternion.Identity;
        }

        private void InputThread()
        {
            bool started = false;
            uint? prevTime = null;

            try
            {
                SerialPort port = new SerialPort("COM7", 115200);
                port.Open();

                while (!_thrdStop)
                {
                    string input;
                    try
                    {
                        input = port.ReadLine();
                    }
                    catch (TimeoutException)
                    {
                        continue;
                    }

                    foreach (string line in input.Split('\r', '\n').Where(x => x.Length > 0))
                    {
                        if (line.FirstOrDefault() != '=')
                            continue;

                        if (!started)
                        {
                            started = true;
                            continue;
                        }

                        string[] tokens = input.Substring(1)
                            .Split(' ', '\t', '\r', '\n').Where(x => x.Length > 0).ToArray();
                        if (tokens.Length != 7)
                            continue;

                        uint micros;
                        double[] values;
                        try
                        {
                            micros = Convert.ToUInt32(tokens[0], 16);
                            values = tokens.Skip(1)
                                .Select(x => (double)uint2float(Convert.ToUInt32(x, 16))).ToArray();
                        }
                        catch (FormatException) { continue; }

                        if (prevTime is null || prevTime.Value >= micros)
                        {
                            prevTime = micros;
                            continue;
                        }

                        double dt = (micros - prevTime.Value) / 1000000.0;
                        prevTime = micros;
                        System.Diagnostics.Debug.Assert(dt < 1000);

                        Dispatcher.InvokeAsync(() =>
                        {
                            double wx = values[3];
                            double wy = values[4];
                            double wz = values[5];
                            var w = new Quaternion(wx, wy, wz, 0);
                            var qdot = Scale(viewRotate.Quaternion * w, 0.5);
                            viewRotate.Quaternion += Scale(qdot, dt);
                            viewRotate.Quaternion = Normalize(viewRotate.Quaternion);

                            accelX.AddValue(dt, values[0]);
                            accelY.AddValue(dt, values[1]);
                            accelZ.AddValue(dt, values[2]);
                            gyroX.AddValue(dt, values[3]);
                            gyroY.AddValue(dt, values[4]);
                            gyroZ.AddValue(dt, values[5]);

                            double acc = Math.Sqrt(values[0] * values[0] + values[1] * values[1] + values[2] * values[2]);
                            double gyro = Math.Sqrt(values[3] * values[3] + values[4] * values[4] + values[5] * values[5]);
                            accelTotal.AddValue(dt, acc);
                            gyroTotal.AddValue(dt, gyro);
                        });
                    }
                }
            }
            catch (InvalidOperationException ex)
            {
                Dispatcher.Invoke(() =>
                {
                    MessageBox.Show("serial communication error: " + ex.Message);
                    _thrd = null;
                    Close();
                });
            }
        }

        public static Quaternion Scale(Quaternion lhs, double rhs)
        {
            return new Quaternion(lhs.X * rhs, lhs.Y * rhs, lhs.Z * rhs, lhs.W * rhs);
        }

        public static Quaternion Normalize(Quaternion q)
        {
            double l = Math.Sqrt(q.X * q.X + q.Y * q.Y + q.Z * q.Z + q.W * q.W);
            return Scale(q, 1 / l);
        }

        private static unsafe float uint2float(uint value)
        {
            return *(float*)(&value);
        }

        private Thread _thrd;
        private volatile bool _thrdStop = false;
    }
}
