# Drone Software

`리모콘 - 중계기 - 드론` 연결에서 중계기에 올라가는 ARDrone SDK 소프트웨어

### 빌드 방법

> Ubuntu 12.04 32bit required

```sh
cd Examples/Linux
make
Build/Release/drone_control
# 의존성은 오류 목록에 뜨므로 알아서 설치
```

### Known Issues

Ubuntu 12.04 32bit 이하 버전에서만 SDK 빌드가 되는데 docker나 virtualbox을 이용할 경우 network-bridge는 bluetooth 연결 안됨, network-host는 drone 연결 안됨
해결방법은 SDK를 사용하지 않고 TCP 패킷을 직접 보내는 라이브러리를 작성해야 함.