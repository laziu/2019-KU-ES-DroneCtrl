#include <ardrone_tool/Navdata/ardrone_navdata_client.h>

#include <stdio.h>
#include <stdlib.h>

#define CTRL_STATES_STRING
#include "control_states.h"

#include "navdata.h"
#include "control.h"
#include "bt_connect.h"

/* Initialization local variables before event loop  */
inline C_RESULT demo_navdata_client_init( void* data )
{
  return C_OK;
}

/* Receving navdata during the event loop */
inline C_RESULT demo_navdata_client_process( const navdata_unpacked_t* const navdata )
{
	const navdata_demo_t*nd = &navdata->navdata_demo;
	const controller_info_t*cs = &controller_state;

	printf("===================== Navdata for flight demonstrations =====================\n");
	printf("Control state : %i                  / Battery level : %i mV\n", nd->ctrl_state, nd->vbat_flying_percentage);
	printf("Altitude      : %i\n",nd->altitude);
	printf("Orientation   : [Theta] %4.3f  [Phi] %4.3f  [Psi] %4.3f\n", nd->theta, nd->phi, nd->psi);
	printf("Speed         :    [vX] %4.3f   [vY] %4.3f   [vZ] %4.3f\n",nd->vx,nd->vy,nd->vz);
	printf("============================= Controller states =============================\n");
	printf("Start : %i                          / Emergency : %i\n", cs->start, cs->emergency);
	printf("[Theta] %4.3f  [Phi] %4.3f  [Gaz] %4.3f  [Yaw] %4.3f  [magnet-Psi] %4.3f\n", cs->theta, cs->phi, cs->gaz, cs->yaw, cs->magneto_psi);
	printf("Mode : [hover] %i  [yaw] %i  [absolute] %i  [psi-accuracy] %4.3f\n", cs->hover_mode, cs->yaw_mode, cs->absolute_mode, cs->magneto_psi_accuracy);
	printf("============================== Bluetooth data ===============================\n");
	printf("microsecond: %d\n", bt_data.microsecond);
	printf("acceleration: [x] %4.3f  [y] %4.3f  [z] %4.3f\n", bt_data.accel_x, bt_data.accel_y, bt_data.accel_z);
	printf("gyrometer:    [x] %4.3f  [y] %4.3f  [z] %4.3f\n", bt_data.gyro_x, bt_data.gyro_y, bt_data.gyro_z);
	printf("scroll:       [x] %4.3f  [y] %4.3f  / click: %i\n", bt_data.scroll_x, bt_data.scroll_y, bt_data.click);

	controller_state.droneglobalcoord.z = nd->altitude;
	controller_state.droneglobalcoord.x += nd->vx * 20 * 0.001// assume nd->theta is in radians, and nd->vx is m/s
	controller_state.droneglobalcoord.y += nd->vy * 20 * 0.001// assume nd-> phi is in radians, and nd->vy is m/s


  return C_OK;
}

/* Relinquish the local resources after the event loop exit */
inline C_RESULT demo_navdata_client_release( void )
{
  return C_OK;
}

/* Registering to navdata client */
BEGIN_NAVDATA_HANDLER_TABLE
  NAVDATA_HANDLER_TABLE_ENTRY(demo_navdata_client_init, demo_navdata_client_process, demo_navdata_client_release, NULL)
END_NAVDATA_HANDLER_TABLE

