#include <ardrone_api.h>
#include <VP_Os/vp_os_print.h>
#include <ardrone_tool/Navdata/ardrone_academy_navdata.h>

#include "control.h"
#include "bt_connect.h"
#include "navdata.h"

C_RESULT open_control_device(void);
C_RESULT update_control_device(void);
C_RESULT close_control_device(void);

input_device_t control_device = {
  "Control",
  open_control_device,
  update_control_device,
  close_control_device
};

controller_info_t controller_state, _z = {};

// TODO: remove this in final; not required
// Get keyboard input; return -1 if no input
#include <termios.h>
#include <unistd.h>

int kbhit(void) {
  struct termios oldt, newt;
  int ch;

  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;

  newt.c_lflag &= ~( ICANON | ECHO );
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  ch = getchar();
  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);

  return ch;
}

C_RESULT open_control_device(void) {
	MTVec3D identity = { 0,0,0 };

  controller_state = _z;
  controller_state.quaternion = mtCreateMTQuaternion(identity, 1.0);
  return C_OK;
}

void convert_coordinates_to_commands(MTVec3D original, MTVec3D destination) 
// Setting the droneglobal coord should be done by touching the ardroneSDK itself or in navdata. 
// Don't know if it's safe to do so.
// So that part is left empty
// Here, the drone just tilts towards the destination and hopes for the best
// assume that positive roll -> drones floats to the right (+x direction)
// positive pitch -> drone floats towards the back (-y direction)
// increased gaz -> drone floats upwards (+z direction)
// just let the drone hover if it's within around 30 centimeters from the position it's supposed to be
{
	int xflag = 0;
	int yflag = 0;
	int zflag = 0;
    #define _INC(_v) controller_state. _v = 0.5f
    #define _DEC(_v) controller_state. _v = -0.5f
    #define _SET(_v) controller_state. _v = 0.0f;
	if (abs((int)(original.x - destination.x)) > 30)
	{
		if (original.x - destination.x > 0.0)
			_INC(phi);
		else
			_DEC(phi);
	}
	else 
	{
		_SET(phi);
		xflag = 1;
	}
	if (abs((int)(original.y - destination.y)) > 30)
	{
		if (original.y - destination.y > 0.0)
			_DEC(theta);
		else
			_DEC(theta);
	}
	else
	{
		_SET(phi);
		yflag = 1;
	}
	if (abs((int)(original.z - destination.z)) > 30)
	{
		if (original.z - destination.z > 0.0)
			_INC(gaz);
		else
			_DEC(gaz);
	}
	else
	{
		_SET(gaz);
		zflag = 1;
	}
    #undef _INC
    #undef _DEC
	if(xflag && yflag && zflag)
	{
		controller_state.hover_mode = 0;
	}
	else
	{
		controller_state.hover_mode = 1;
	}
};


C_RESULT update_control_device(void) {
	double dt = (bt_data.microseconds - bt_data.prevtime) / 1000000.0;
	
	double wx = bt_data.gyro_x[3];
	double wy = bt_data.gyro_y[4];
	double wz = bt_data.gyro_z[5];
	controller_state.r += bt_data.scroll.x;

	MTVec3D vector = { wx, wy, wz };
	MTQuaternion q = mtCreateMTQuaternion(vector, 0.0);
	MtQuaternion qdot = mtMultMTQuaternionScalar(mtMultMTQuaternionMTQuaternion(controller_state.quaternion,q), 0.5);
	qdot = mtMultMTQuaternionScalar(qdot, dt);
	controller_state.quaternion = mtAddMTQuaternionMTQuaternion(controller_state.quaternion, qdot);
	controller_state.quaternion = mtNormMTQuaternion(controller_state.quaternion);

	MTVec3D destination = { 1,0,0 };
	destination = mtRotatePointWithMTQuaternion(controller_state.quaternion, destination);
	destination = mtMultiplyVectorScalar(destination, uint2float(r));

	convert_coordinates_to_commands(controller_state.droneglobalcoord, destination);

	
	/*int key = kbhit();

  if (key == '1') {
    controller_state.start = !controller_state.start;
    ardrone_tool_set_ui_pad_start(controller_state.start);
    //ardrone_academy_navdata_takeoff();
  }
  else if (key == '2') {
    controller_state.emergency = !controller_state.emergency;
    ardrone_tool_set_ui_pad_select(controller_state.emergency);
    //ardrone_academy_navdata_emergency();
  }
  else {
    #define _INC(_c, _v) case _c : controller_state. _v += 0.5f; if ( controller_state. _v >  1000000.0f) controller_state. _v =  1.0f; break
    #define _DEC(_c, _v) case _c : controller_state. _v -= 0.5f; if ( controller_state. _v < -1000000.0f) controller_state. _v = -1.0f; break
    switch (key) {
      _INC('q', phi);
      _INC('w', theta);
      _INC('e', gaz);
      _INC('r', yaw);
      _INC('t', magneto_psi);
      _INC('y', magneto_psi_accuracy);

      _DEC('a', phi);
      _DEC('s', theta);
      _DEC('d', gaz);
      _DEC('f', yaw);
      _DEC('g', magneto_psi);
      _DEC('h', magneto_psi_accuracy);

      case '3': controller_state.hover_mode = !controller_state.hover_mode; break;
      case '4': controller_state.yaw_mode = !controller_state.yaw_mode; break;
      case '5': controller_state.absolute_mode = !controller_state.absolute_mode; break;
      default: key = -1;
    }
    #undef _INC
    #undef _DEC*/
      int32_t flags = (controller_state.hover_mode ? 1 : 0) | (controller_state.yaw_mode ? 2 : 0) | (controller_state.absolute_mode ? 4 : 0);
      ardrone_tool_set_progressive_cmd(
        flags, 
        controller_state.phi, // roll
        controller_state.theta, // pitch
        controller_state.gaz,
        controller_state.yaw,
        controller_state.magneto_psi, 
        controller_state.magneto_psi_accuracy
      );
  }
  
  return C_OK;
}

C_RESULT close_control_device(void) {
  return C_OK;
}
