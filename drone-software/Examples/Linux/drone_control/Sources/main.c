/**
 * @file main.c
 * @author sylvain.gaeremynck@parrot.com
 * @date 2009/07/01
 */
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <VP_Os/vp_os_types.h>

//ARDroneLib
#include <utils/ardrone_time.h>
#include <ardrone_tool/Navdata/ardrone_navdata_client.h>
#include <ardrone_tool/Control/ardrone_control.h>
#include <ardrone_tool/UI/ardrone_input.h>

//Common
#include <config.h>
#include <ardrone_api.h>

//VP_SDK
#include <ATcodec/ATcodec_api.h>
#include <VP_Os/vp_os_print.h>
#include <VP_Api/vp_api_thread_helper.h>
#include <VP_Os/vp_os_signal.h>

//Local project
#include "control.h"
#include "video_stage.h"
#include "bt_connect.h"

static int32_t exit_ihm_program = 1;

C_RESULT signal_exit()
{
  exit_ihm_program = 0;
  return C_OK;
}

void controlCHandler (int signal)
{
	static int callCounter=0;

	callCounter++;

	/* In case in unresponsive, force killing the application */
	if(callCounter>3){
		printf("Ctrl-C pressed several times. Killing the application ...\n");
		exit(-1);
	}

#if CONTROL_C_HANDLER_USES_ARDRONE_TOOL_EXIT
    signal_exit ();
#else
    // Flush all streams before terminating
    fflush (NULL);
    usleep (200000); // Wait 200 msec to be sure that flush occured
    printf ("\nAll files were flushed\n");
    exit (0);
#endif
}

/* Implementing Custom methods for the main function of an ARDrone application */
int main(int argc, char** argv)
{
  signal (SIGABRT, &controlCHandler);
  signal (SIGTERM, &controlCHandler);
  signal (SIGINT, &controlCHandler);
	return ardrone_tool_main(argc, argv);
}

/* The delegate object calls this method during initialization of an ARDrone application */
C_RESULT ardrone_tool_init_custom(void)
{
  /* Registering for a new device of game controller */
  ardrone_tool_input_add(&control_device);

  /* Start all threads of your application */
  START_THREAD( video_stage, NULL );
  START_THREAD( bluetooth_connect, NULL );
  
  return C_OK;
}

/* The delegate object calls this method when the event loop exit */
C_RESULT ardrone_tool_shutdown_custom(void)
{
  /* Relinquish all threads of your application */
  JOIN_THREAD( video_stage );
  JOIN_THREAD( bluetooth_connect );

  /* Unregistering for the current device */
  ardrone_tool_input_remove(&control_device);

  return C_OK;
}

/* The event loop calls this method for the exit condition */
bool_t ardrone_tool_exit()
{
  return exit_ihm_program == 0;
}

/* Implementing thread table in which you add routines of your application and those provided by the SDK */
BEGIN_THREAD_TABLE
  THREAD_TABLE_ENTRY( ardrone_control, 20 )
  THREAD_TABLE_ENTRY( navdata_update, 20 )
  THREAD_TABLE_ENTRY( bluetooth_connect, 20 )
  THREAD_TABLE_ENTRY( video_stage, 20 )
END_THREAD_TABLE

