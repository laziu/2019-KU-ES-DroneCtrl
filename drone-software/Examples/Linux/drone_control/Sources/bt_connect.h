#ifndef _BT_CONNECT_H_
#define _BT_CONNECT_H_

#include <config.h>
#include <VP_Api/vp_api_thread_helper.h>

typedef struct _bt_data_t {
    uint32_t microsecond;
	uint32_t prevtime;
    float32_t accel_x;
    float32_t accel_y;
    float32_t accel_z;
    float32_t gyro_x;
    float32_t gyro_y;
    float32_t gyro_z;
    int32_t scroll_x;
    int32_t scroll_y;
    bool_t click;
} bt_data_t;

extern bt_data_t bt_data;

PROTO_THREAD_ROUTINE( bluetooth_connect, data );

#endif // _BT_CONNECT_H_