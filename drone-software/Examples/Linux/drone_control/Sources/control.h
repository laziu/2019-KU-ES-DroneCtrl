#ifndef _CONTROL_H_
#define _CONTROL_H_

#include <ardrone_tool/UI/ardrone_input.h>
#include "mtQuaternions.h"

extern input_device_t control_device;

typedef struct _controller_info_t {
  int32_t start;
  int32_t emergency;
  int32_t hover_mode;
  int32_t yaw_mode;
  int32_t absolute_mode;
  float32_t phi;
  float32_t theta;
  float32_t gaz;
  float32_t yaw;
  float32_t magneto_psi;
  float32_t magneto_psi_accuracy;
  MTQuaternion quaternion;
  int32_t r;
  MTVec3D droneglobalcoord;
} controller_info_t;

extern controller_info_t controller_state;

#endif // _CONTROL_H_