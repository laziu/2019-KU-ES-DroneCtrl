# Drone Controller
아두이노를 이용한 컨트롤러.

## 배선
### MPU-9250 연결
| MPU9250 | Arduino Nano |
|:-------:|:------------:|
| VCC     | 5V           |
| GND     | GND          |
| SCL     | A5           |
| SDA     | A4           |

### HC-06 연결
| HC-06 | Arduino Nano |
|:-----:|:------------:|
| VCC   | 5V           |
| GND   | GND          |
| RXD   | 3            |
| TXD   | 2            |

### 조이스틱 연결
| Joystick | Arduino Nano |
|:--------:|:------------:|
| +5V      | 5V           |
| GND      | GND          |
| VRx      | A2           |
| VRy      | A1           |
| SW       | 5            |

### 배터리 연결
| Battery   | Arduino Nano |
|:---------:|:------------:|
| +         | 5V           |
| -         | GND          |