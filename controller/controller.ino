#include <SoftwareSerial.h>
#include "MPU9250.h"

const int btRx = 3, btTx = 2, sw = 5;
const int x = A1, y = A2;
SoftwareSerial bt(btTx, btRx);

MPU9250 IMU(Wire,0x68);
int status, xx, yy, swsw;

void setup()
{
  Serial.begin(9600);
  pinMode(x, INPUT);
  pinMode(y, INPUT);
  pinMode(sw, INPUT);
  digitalWrite(sw, HIGH);
  while(!Serial) {}
  bt.begin(9600);
  bt.println("AT+NAMEdronermctrl");

  status = IMU.begin();
  if (status < 0)
  {
    Serial.print("IMU Error: ");
    Serial.println(status);
    while(1) { }
  }
}

void writeulong(unsigned long x)
{
  Serial.print(x, HEX);
  Serial.print(" ");
  bt.print(x, HEX);
  bt.print(" ");
}

void writefloat(float x)
{
  byte* ptr = (byte*)&x;
  unsigned long v = ((unsigned long)ptr[3] << 24) | ((unsigned long)ptr[2] << 16) | ((unsigned long)ptr[1] << 8) | ptr[0];
  Serial.print(v, HEX);
  Serial.print(" ");
  bt.print(v, HEX);
  bt.print(" ");
}

void loop()
{
  xx = analogRead(x);
  yy = analogRead(y);
  swsw = digitalRead(sw);
  IMU.readSensor();
  Serial.print("=");
  bt.print("=");
  writeulong(micros());
  writefloat(IMU.getAccelX_mss());
  writefloat(IMU.getAccelY_mss());
  writefloat(IMU.getAccelZ_mss());
  writefloat(IMU.getGyroX_rads());
  writefloat(IMU.getGyroY_rads());
  writefloat(IMU.getGyroZ_rads());
  Serial.print(xx);
  Serial.print(" ");
  Serial.print(yy);
  Serial.print(" ");
  Serial.print(swsw);
  bt.print(xx);
  bt.print(" ");
  bt.print(yy);
  bt.print(" ");
  bt.print(swsw);
  Serial.println();
  bt.println();
  delay(100);
}
